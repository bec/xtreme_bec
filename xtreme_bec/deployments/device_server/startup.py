import os


def setup_epics_ca():
    os.environ["EPICS_CA_AUTO_ADDR_LIST"] = "NO"
    os.environ["EPICS_CA_ADDR_LIST"] = "129.129.112.255 sls-x07ma-cagw.psi.ch:5836"
    os.environ["PYTHONIOENCODING"] = "latin1"


def run():
    setup_epics_ca()
